package trabajoIndividialProcesadorDeTexto;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledEditorKit;

import trabajoIndividualBanco.CurrentAccount;

public class MainPanel extends JPanel {

	private JTextPane textArea;
	private JMenu file;
	private JMenu font;
	private JMenu style;
	private JMenu aligment;
	private JMenu size;
	private JMenu colors;

	public MainPanel() {
		setLayout(new BorderLayout());

		JPanel panelMenu = new JPanel();
		JMenuBar myMenu = new JMenuBar();
		this.file = new JMenu("Archivo");
		this.font = new JMenu("Fuente");
		this.style = new JMenu("Estilo");
		this.aligment = new JMenu("Alineaci�n");
		this.size = new JMenu("Tama�o");
		this.colors = new JMenu("Color");

		menuConfiguration("Abrir", "file", "", 0, 0);
		menuConfiguration("Guardar", "file", "", 0, 0);

		menuConfiguration("Sans Serif", "font", "Sans Serif", 9, 10);
		menuConfiguration("Arial", "font", "Arial", 9, 10);
		menuConfiguration("Courier", "font", "Courier", 9, 10);
		menuConfiguration("Verdana", "font", "Verdana", 9, 10);

		menuConfiguration("Negrita", "style", "", Font.BOLD, 1);
		menuConfiguration("Cursiva", "style", "", Font.ITALIC, 1);

		ButtonGroup letterSize = new ButtonGroup();
		JRadioButtonMenuItem num_12 = new JRadioButtonMenuItem("12");
		JRadioButtonMenuItem num_14 = new JRadioButtonMenuItem("14");
		JRadioButtonMenuItem num_20 = new JRadioButtonMenuItem("20");
		JRadioButtonMenuItem num_24 = new JRadioButtonMenuItem("24");
		JRadioButtonMenuItem num_36 = new JRadioButtonMenuItem("36");
		JRadioButtonMenuItem num_42 = new JRadioButtonMenuItem("42");

		letterSize.add(num_12);
		letterSize.add(num_14);
		letterSize.add(num_20);
		letterSize.add(num_24);
		letterSize.add(num_36);
		letterSize.add(num_42);
		num_12.addActionListener(new StyledEditorKit.FontSizeAction("modifySize", 12));
		num_14.addActionListener(new StyledEditorKit.FontSizeAction("modifySize", 14));
		num_20.addActionListener(new StyledEditorKit.FontSizeAction("modifySize", 20));
		num_14.addActionListener(new StyledEditorKit.FontSizeAction("modifySize", 24));
		num_36.addActionListener(new StyledEditorKit.FontSizeAction("modifySize", 36));
		num_42.addActionListener(new StyledEditorKit.FontSizeAction("modifySize", 42));

		size.add(num_12);
		size.add(num_14);
		size.add(num_20);
		size.add(num_24);
		size.add(num_36);
		size.add(num_42);

		menuConfiguration("center", "aligment", "", 0, 0);
		menuConfiguration("right", "aligment", "", 0, 0);
		menuConfiguration("left", "aligment", "", 0, 0);

		menuConfiguration("Verde", "colors", "", 0, 0);
		menuConfiguration("Rojo", "colors", "", 0, 0);
		menuConfiguration("Amarillo", "colors", "", 0, 0);
		menuConfiguration("Gris", "colors", "", 0, 0);
		menuConfiguration("Rosado", "colors", "", 0, 0);
		menuConfiguration("Azul", "colors", "", 0, 0);

		myMenu.add(file);
		myMenu.add(font);
		myMenu.add(style);
		myMenu.add(size);
		myMenu.add(aligment);
		myMenu.add(colors);

		panelMenu.add(myMenu);

		add(panelMenu, BorderLayout.NORTH);

		textArea = new JTextPane();

		add(textArea, BorderLayout.CENTER);

		JPopupMenu popupMenu = new JPopupMenu();

		JMenuItem bold = new JMenuItem("Negrita");

		JMenuItem italic = new JMenuItem("Cursiva");

		bold.addActionListener(new StyledEditorKit.BoldAction());

		italic.addActionListener(new StyledEditorKit.ItalicAction());

		popupMenu.add(bold);

		popupMenu.add(italic);

		textArea.setComponentPopupMenu(popupMenu);

	}

	public void menuConfiguration(String txtMenu, String menu, String word, int styles, int letterSize) {

		JMenuItem item_menu = new JMenuItem(txtMenu);

		if (menu == "file") {
			file.add(item_menu);

			if (txtMenu == "Abrir") {
				item_menu.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						chooseWindow();
					}
				});
			} else if (txtMenu == "Guardar") {
				item_menu.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						saveFile();
					}
				});
			}

		} else if (menu == "font") {

			font.add(item_menu);

			if (word == "Sans Serif") {

				item_menu.addActionListener(new StyledEditorKit.FontFamilyAction("modifyLetter", "Sans Serif"));
			} else if (word == "Arial") {

				item_menu.addActionListener(new StyledEditorKit.FontFamilyAction("modifyLetter", "Arial"));
			} else if (word == "Courier") {

				item_menu.addActionListener(new StyledEditorKit.FontFamilyAction("modifyLetter", "Courier"));
			} else if (word == "Verdana") {

				item_menu.addActionListener(new StyledEditorKit.FontFamilyAction("modifyLetter", "Verdana"));
			}

		} else if (menu == "style") {

			style.add(item_menu);

			if (styles == Font.BOLD) {

				item_menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));

				item_menu.addActionListener(new StyledEditorKit.BoldAction());

			} else if (styles == Font.ITALIC) {

				item_menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_DOWN_MASK));

				item_menu.addActionListener(new StyledEditorKit.ItalicAction());

			}

		} else if (menu == "size") {

			size.add(item_menu);

			item_menu.addActionListener(new StyledEditorKit.FontSizeAction("modifySize", letterSize));

		} else if (menu == "aligment") {

			aligment.add(item_menu);
			if (txtMenu == "center") {

				item_menu.setText("Centrado");
				item_menu.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						SimpleAttributeSet attribs = new SimpleAttributeSet();
						StyleConstants.setAlignment(attribs, StyleConstants.ALIGN_CENTER);
						textArea.setParagraphAttributes(attribs, true);
					}
				});

			} else if (txtMenu == "left") {

				item_menu.setText("Izquierda");
				item_menu.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						SimpleAttributeSet attribs = new SimpleAttributeSet();
						StyleConstants.setAlignment(attribs, StyleConstants.ALIGN_LEFT);
						textArea.setParagraphAttributes(attribs, true);
					}
				});
			} else if (txtMenu == "right") {

				item_menu.setText("Derecha");
				item_menu.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						SimpleAttributeSet attribs = new SimpleAttributeSet();
						StyleConstants.setAlignment(attribs, StyleConstants.ALIGN_RIGHT);
						textArea.setParagraphAttributes(attribs, true);
					}
				});
			}

		} else if (menu == "colors") {
			colors.add(item_menu);
			if (txtMenu == "Verde") {

				item_menu.addActionListener(new StyledEditorKit.ForegroundAction("", Color.GREEN));
			} else if (txtMenu == "Rojo") {

				item_menu.addActionListener(new StyledEditorKit.ForegroundAction("", Color.RED));
			} else if (txtMenu == "Amarillo") {

				item_menu.addActionListener(new StyledEditorKit.ForegroundAction("", Color.YELLOW));
			} else if (txtMenu == "Gris") {

				item_menu.addActionListener(new StyledEditorKit.ForegroundAction("", Color.GRAY));
			} else if (txtMenu == "Rosado") {

				item_menu.addActionListener(new StyledEditorKit.ForegroundAction("", Color.PINK));
			} else if (txtMenu == "Azul") {

				item_menu.addActionListener(new StyledEditorKit.ForegroundAction("", Color.BLUE));
			}
		}

	}

	private void chooseWindow() {

		JFileChooser fc = new JFileChooser();

		FileNameExtensionFilter filter = new FileNameExtensionFilter("*.TXT", "txt");

		fc.setFileFilter(filter);

		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);

		int selection = fc.showOpenDialog(this);

		if (selection == JFileChooser.APPROVE_OPTION) {

			File file = fc.getSelectedFile();

			try (FileReader fr = new FileReader(file)) {
				String text = "";
				int value = fr.read();
				while (value != -1) {
					text = text + (char) value;
					value = fr.read();
				}
				textArea.setText(text);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

	}

	private void saveFile() {
		JFileChooser fc = new JFileChooser();

		FileNameExtensionFilter filter = new FileNameExtensionFilter("*.TXT", "txt");

		fc.setFileFilter(filter);

		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		
		int userSelection = fc.showSaveDialog(this);
		String absolutePath = null;
		
		if (userSelection == JFileChooser.APPROVE_OPTION) {
		    File fileToSave = fc.getSelectedFile();
		    
		    if(fileToSave.getAbsolutePath().contains(".")) {
		    	
		    	if(fileToSave.getAbsolutePath().endsWith(".")) {
		    		absolutePath = fileToSave.getAbsolutePath() + "txt";
		    	}else {

		    		String [] allPath = fileToSave.getAbsolutePath().split("\\.");
			    	
			    	if(allPath[allPath.length-1] == ".") {

			    		absolutePath = fileToSave.getAbsolutePath() + "txt";
			    	}else {

			    		if(!allPath[allPath.length-1].equals("txt")) {
			    			absolutePath = fileToSave.getAbsolutePath() + ".txt";	
			    		}else {
			    			absolutePath = fileToSave.getAbsolutePath();
			    		}
			    	}
		    	}
		    	
		    }else {
		    	absolutePath = fileToSave.getAbsolutePath() + ".txt";
		    }
		  
		    try {
	            FileWriter fw = new FileWriter(absolutePath);
	            fw.write(textArea.getText());
	            fw.close();

	        } catch (IOException iox) {

	            iox.printStackTrace();
	        }
		}
	}

	public String getTextFromTextArea() {
		return textArea.getText();
	}
	
}