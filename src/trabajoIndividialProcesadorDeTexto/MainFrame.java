package trabajoIndividialProcesadorDeTexto;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class MainFrame extends JFrame{
	
	private MainPanel layer; 
	
	public MainFrame() {
		
		setBounds(500,300,550,420);
        layer = new MainPanel();
        JScrollPane scrollPane = new JScrollPane(layer);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        add(scrollPane);
        super.setTitle("Procesador de Texto");
        this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				saveFile();
				super.windowClosing(e);
			}
        	
		});
        setVisible(true);

	}
	
	private void saveFile() {
		  
		    try {
	            File newTextFile = new File("autoSave.txt");
	            System.out.println("Se guardo por defecto en " + newTextFile.getAbsolutePath());
	            FileWriter fw = new FileWriter(newTextFile.getAbsolutePath());
	            fw.write(this.layer.getTextFromTextArea());
	            fw.close();

	        } catch (IOException iox) {

	            iox.printStackTrace();
	        }
	}
	
}
