package desafio11;

public class UsoCuenta {

	public static void main (String[] args) {
		
		CuentaCorriente c1 = new CuentaCorriente("Correa Carla", 38000);
		CuentaCorriente c2 = new CuentaCorriente("Monzon Oscar", 150);
		
		System.out.println(c1.toString());
		System.out.println(c2.toString());
		
		c1.transferencia(c2, 2500);
		
		System.out.println(c1.toString());
		System.out.println(c2.toString());
		
		c1.transferencia(c2, 40000);
								
	}
}
