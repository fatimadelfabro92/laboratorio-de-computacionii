package desafio11;

import java.util.Random;

public class CuentaCorriente {

	private double saldo;
	private String nombreTitular;
	private long nroCuenta;
	private Random random = new Random();
	
	public CuentaCorriente(String nombreTitular, double saldo) {
		this.nombreTitular = nombreTitular;
		this.saldo = saldo;
		this.nroCuenta = Math.abs(random.nextLong());
		
	}
	
	public String getnombreTitular() {
		return nombreTitular;
	}

	public Double getSaldo() {
		return saldo;
	}

	public long getNroCuenta() {
		return nroCuenta;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	
	public void setNroCuenta(long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	
	public void transferencia(CuentaCorriente cuentaX, double transf) {
		
		if(this.saldo >= transf) {
			this.saldo = this.saldo - transf;
			cuentaX.saldo = cuentaX.saldo + transf;
			
			System.out.println("La transaccion se ha realizadoo con exito!");
			
		}else {
			System.out.println("Lo sentimos, Usted no esta habilitado para reaizar la transacción.");
		}
		
	}
	
	@Override
	public String toString() {
		return "------------------------------" +
	"\nDatos de la cuenta:" +
	"\nTitular " + nombreTitular + 
	"\nDisponibe: " + saldo + 
	"\nNumero de cuenta: " + nroCuenta +
	"\n------------------------------";
	}
}
