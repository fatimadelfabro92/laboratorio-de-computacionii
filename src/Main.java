import java.util.Scanner;

public class Main {
 
	public static void main(String[] args) {
		
		String nombre;
		int edad;
		double salario;
		boolean carnet;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Ingrese por favor su nombre");
		
		nombre = sc.nextLine();
		
		System.out.println("Bienvenido/a " + nombre + " a 'TUPRIMER0Km!'. Para verificar si eres apto/a para adquirir un plan, necesitamos que nos informes unos datos..");
		
		System.out.println("Ingrese su edad");
		
		edad = sc.nextInt();
		
		if(edad >= 18) {
			System.out.println("Genial " + nombre + " eres mayor de edad. Estas a un paso m�s cerca de adquirir un maravilloso plan para tener tu primer 0Km! ");
			
			System.out.println("Ahora por favor ingresa el importe de tu salario");
			
			salario = sc.nextDouble();
			
			if(salario > 10000) {
				System.out.println("Queremos saber si cuentas con carnet de conducir. Ingresa con 1 o 2 seg�n corresponda "
						+ "\n 1)SI \n 2)NO");
				
				int op = sc.nextInt();
				
				if(op == 1) {
					carnet = true;
					
				}else {
					carnet = false;
				}
				
				if(!carnet) {
					System.out.println(nombre + " no te preocupes en 'TUPRIMER0Km!' contamos con asesoramiento para que puedas obtener tu licencia de conducir. "
							+ "\nAdemas queremos felicitarte porque has sido calificado/a de manera correcta para adquirir un plan para tu primer 0Km 100% financiado!");
				}else {
					System.out.println("Wow " + nombre + ", felicitaciones has sido calificado/a de manera correcta para adquirir un plan para tu primer 0Km 100% financiado!");
				}
				
			}else {
				System.out.println("Lo sentimos " + nombre + " no tenemos un plan para ofrecerte.");
			}
			
		}else { 
			System.out.println("Lo sentimos " + nombre + " no tenemos un plan para ofrecerte en estos momentos.");
		}
		
	}

}
