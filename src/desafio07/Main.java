package desafio07;

import java.text.DecimalFormat;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class Main {
	public static void main (String[] args) {
		
		String op = JOptionPane.showInputDialog("Inquede el nro seg�n la operacion que quiere realizar"
				+ "\nFUNCIONES TRIGONOM�TRICAS"
				+ "\n1) Calcular el seno trigonom�trico de un �ngulo."
				+ "\n2) Calcular el coseno trigonom�trico de un �ngulo." 
				+ "\n3) Calcular la tangente trigonom�trica de un �ngulo."
				+ "\n4) Calcular el arco tangente de un valor"
				+ "\n5) Calcular el �ngulo theta de la conversi�n de coordenadas rectangulares (x, y) a coordenadas polares (r, theta)."
				+ "\nFUNCI�N EXPONENCIAL Y SU INVERSA"
				+ "\n6) Calcular el n�mero de Elur 'e' elevado a la potencia de un valor doble"
				+ "\n7) Calcular el algoritmo natural (base e) de un valor doble"
				+ "\n8) MOSTRAR EL VALOR DE PI"
				+ "\n9) MOSTRAR EL VALOR DE E");
		
		int opNumero = Integer.parseInt(op);
		double x, y;
		Scanner sc = new Scanner(System.in); 
		DecimalFormat df = new DecimalFormat("0.00#");
		
		switch (opNumero) {
		case 1:
			System.out.println("Ingrese el valor del �ngulo de su tri�ngulo");
			x = sc.nextDouble();
			System.out.println("El valor del seno trigonom�trico de su �ngulo es: " + df.format(Math.sin(x)));
			
			break;
		case 2:
			System.out.println("Ingrese el valor del �ngulo de su tri�ngulo");
			x = sc.nextDouble();
			System.out.println("El valor del coseno trigonom�trico de su �ngulo es: " + df.format(Math.cos(x)));
			break;
		case 3:
			System.out.println("Ingrese el valor del �ngulo de su tri�ngulo");
			x = sc.nextDouble();
			System.out.println("El valor de la tangente trigonom�trica de su �ngulo es: " + df.format(Math.tan(x)));
			break;
		case 4:
			System.out.println("Ingrese el valor sobre el que se va a calcular el arco tangente");
			x = sc.nextDouble();
			System.out.println("El valor del arco tangente en base a su valor ingresado es: " + df.format(Math.atan(x)));
			break;
		case 5:
			System.out.println("Ingrese los dos valores necesarios para su posterior calculo");
			System.out.println("Valor 1: ");
			x = sc.nextDouble();
			System.out.println("Valor 2: ");
			y = sc.nextDouble();
			System.out.println("El resultado seg�n los datos ingresados es: " + df.format(Math.atan2(x, y)));
			break;
		case 6:
			System.out.println("Ingrese el valor sobre el cual quiere realizar su calculo");
			x = sc.nextDouble();
			System.out.println("El resultado de su operaci�n es: " + df.format(Math.exp(x)));
			break;
		case 7:
			System.out.println("Ingrese el valor sobre el cual quiere realizar su calculo");
			x = sc.nextDouble();
			System.out.println("El resultado de su operaci�n es: " + df.format(Math.log(x)));
			break;
		case 8:
			System.out.println("El valor de PI es: " + df.format(Math.PI));
			break;

		case 9:
			System.out.println("El valor de E es: " + df.format(Math.E));
			break;
		default:
			System.out.println("Usted a ingresado una opci�n invalida.");
			break;
		}
		
	}
}

