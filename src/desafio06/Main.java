package desafio06;

public class Main {
	public static void main(String[] args) {
		
		Fraccion f1 = new Fraccion(6, 5);
		Fraccion f2 = new Fraccion(2, 3);
		Fraccion f3 = new Fraccion(7, 2);
		Fraccion f4 = new Fraccion(2, 30);
		Fraccion f5 = new Fraccion(1, 3);
		Fraccion f6 = new Fraccion(1, 5);
		
		Fraccion numerador = f1.restar(f2.multiplicar(f3)).sumar(f4);
		Fraccion den = f5.dividir(f6);
		
		Fraccion total = numerador.dividir(den);
		
		System.out.println("Resultado: " + total);
	}

	
}

class Fraccion {

    private int num;
    private int den;

    public Fraccion() {
        this.num = 0;
        this.den = 1;
    }

    public Fraccion(int num, int den) {
        this.num = num;
        if(den==0){
            den = 1;
        }
        this.den = den;
        simplificar();
    }

    public Fraccion(int num) {
        this.num = num;
        this.den = 1;
    }

    public int getDen() {
        return den;
    }

    public void setDen(int den) {
        this.den = den;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    
    public Fraccion sumar(Fraccion f) {
        Fraccion aux = new Fraccion();
        aux.num = this.num * f.den + this.den * f.num;
        aux.den = this.den * f.den;
        aux.simplificar();                                                 
        return aux;
    }
   
   
    public Fraccion restar(Fraccion f) {
        Fraccion aux = new Fraccion();
        aux.num = this.num * f.den - this.den * f.num;
        aux.den = this.den * f.den;
        aux.simplificar();  
        return aux;
    }
   
   
    public Fraccion multiplicar(Fraccion f) {
        Fraccion aux = new Fraccion();
        aux.num = this.num * f.num;
        aux.den = this.den * f.den;
        aux.simplificar();  
        return aux;
    }

    
    public Fraccion dividir(Fraccion f) {
        Fraccion aux = new Fraccion();
        aux.num = this.num * f.den;
        aux.den = this.den * f.num;
        aux.simplificar();  
        return aux;
    }
   
              
    private int mcd() {
        int u = Math.abs(num);
        int v = Math.abs(den); 
        if (v == 0) {
            return u;
        }
        int r;
        while (v != 0) {
            r = u % v;
            u = v;
            v = r;
        }
        return u;
    }

    
    private void simplificar() {
        int n = mcd();
        num = num / n;
        den = den / n;
    }

    @Override
    public String toString() {
        simplificar();
        return num + "/" + den;
    }    
}
