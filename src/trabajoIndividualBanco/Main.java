package trabajoIndividualBanco;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	static Scanner sc = new Scanner(System.in);
	static List<CurrentAccount> bank;
	static int op=0, err;
	
	public static void main(String[] args) {

		bank = new ArrayList<CurrentAccount>();
		
		
		
		do {
			
			System.out.println("Menu:\n"
					+ "1)Crear una cuenta.\n"
					+ "2)Depositar.\n"
					+ "3)Realizar una extracci�n.\n"
					+ "4)Realizar una transferencia.\n"
					+ "5)Ver saldo.\n"
					+ "6)Informaci�n de cuenta.\n"
					
					+ "0)Salir.");
			
			do {
				try {
					op = getScanner(sc).nextInt();
					err=0;
				}catch(Exception e) {
					System.out.println("ERROR! Ingrese un valor n�merico y que el mismo se encuentre contemplado en el men�.");
					System.out.println("INTENTELO DE NUEVO");
					err=-1;
				}

			}while(err==-1);
			
			switch(op) {
				case 1:
			
					createAnAccount(bank);
				break;
				case 2:
					
					depositInAccount();
				break;
				case 3:
					
					extractFromAccount();
				break;
				case 4:
				
					transferBetweenAccounts();
				break;
				case 5:
				
					available();
				break;
				case 6:
					
					infoAbout();
				break;
				case 0:
					System.out.println("generando archivo.");
					writeAndExit();
				break;
				default:
					System.out.println("Opcion invalida!.");
			}
			
		}while( op != 0 );
		
		
	}
	
	private static void writeAndExit() {
		try {
	      File myObj = new File("bankAccounts.txt");
	      myObj.createNewFile();
	      
	      FileWriter myWriter = new FileWriter("bankAccounts.txt");
	      
	      StringBuilder sb = new StringBuilder();
	      
	      for (CurrentAccount currentAccount : bank) {
	    	  sb.append(currentAccount);
	    	  sb.append("\n");
	      }
	      
	      myWriter.write(sb.toString());
	      
	      
	      myWriter.close();
	      System.out.println("Cerrando.....");
	      
	    } catch (IOException e) {
	      System.out.println("Ha ocurrido un error.");
	      e.printStackTrace();
	    }
		
	}

	private static Scanner getScanner(Scanner sc) {
		sc = null;
		return new Scanner(System.in);
	}

	private static void infoAbout() {
		System.out.println("Ingrese el numero de la cuenta.\n");
		int accountNum = sc.nextInt();
		CurrentAccount ca = searchAccount(accountNum);
		
		if(ca != null) {
			System.out.println(ca +" \n");			
		}
		System.out.println("Verifique los datos ingresados y vuelva a intentar mas tarde......\n");
		
	}

	private static void available() {
		System.out.println("Ingrese el numero de la cuenta.\n");
		int accountNum = sc.nextInt();
		CurrentAccount ca = searchAccount(accountNum);
		
		if(ca != null) {
			System.out.println(ca.balanceInformation() +" \n");
			return;
		}
		System.out.println("Verifique los datos ingresados y vuelva a intentar mas tarde......\n");
	}

	private static void transferBetweenAccounts() {
		System.out.println("Ingrese el numero de la cuenta origen.\n");
		int accountNum = sc.nextInt();
		CurrentAccount from = searchAccount(accountNum);
		
		System.out.println("Ingrese el numero de la cuenta destino.\n");
		accountNum = sc.nextInt();
		CurrentAccount to = searchAccount(accountNum);
		
		if(from != null && to != null && !from.equals(to)) {
			System.out.println("Saldo actual : "+ from.getInitialAccount() +" \n");
			System.out.println("Ingrese el monto a transferir.\n");
			double amount = sc.nextDouble();
			boolean result = from.transfer(to, amount);
			
			if(result) {
				System.out.println("Monto transferido : " + amount + " Desde C.N� : " + from.getAcconuntNumber() + " a destino C.N� : " + to.getAcconuntNumber() + ".\n");
				return;
			}else {
				System.out.println("Ocurrio un error.");
				return;
			}
		}
		
		System.out.println("Verifique los datos ingresados y vuelva a intentarlo.\n");
	}

	private static void extractFromAccount() {
		System.out.println("Ingrese el numero de la cuenta.\n");
		int accountNum = sc.nextInt();
		CurrentAccount ca = searchAccount(accountNum);
		
		if(ca != null) {
			System.out.println("Saldo actual : "+ ca.getInitialAccount() +" \n");
			System.out.println("Ingrese el monto a extraer.\n");
			double amount = sc.nextDouble();
			boolean result = ca.withdrawals(amount);
			
			if(result) {
				System.out.println("Monto retirado : " + amount + "\n");
				return;
			}else {
				System.out.println("Verifique el monto ingresado.\n");
				return;
			}
			
		}
		System.out.println("Vuelva a intentar mas tarde......\n");
	}

	private static void depositInAccount() {
		System.out.println("Ingrese el numero de la cuenta.\n");
		int accountNum = sc.nextInt();
		CurrentAccount ca = searchAccount(accountNum);
		
		if(ca != null) {
			System.out.println("Ingrese el monto a depositar.\n");
			double amount = sc.nextDouble();
			ca.deposit(amount);
			System.out.println("Monto depositado : " + amount+ "\n");
			return;
		}
		System.out.println("Vuelva a intentar mas tarde......\n");
	}
	
	private static CurrentAccount searchAccount(int accountNumber) {
		for (CurrentAccount ca : bank) {
			if(Integer.parseInt(ca.getAcconuntNumber()) == accountNumber) {
				return ca;
			}
		}
		
		return null;
	}

	private static void createAnAccount(List<CurrentAccount> bank) {
		System.out.println("Ingrese el nombre del titular de cuenta.\n");
		String name = getScanner(sc).nextLine();
		
		System.out.println("Ingrese el monto inicial de la cuenta.\n");
		double amount = getScanner(sc).nextDouble();
		
		CurrentAccount account = new CurrentAccount(name, amount);
		
		bank.add(account);
		
		System.out.println("Cuenta Creada N� : " + account.getAcconuntNumber()+ "\n");
	}

}
