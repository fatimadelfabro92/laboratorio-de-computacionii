package trabajoIndividualBanco;

public class CurrentAccount {

	private static int accountCounter = 0;
	private String name;
	private double initialAccount;
	private int acconuntNumber;
	
	public CurrentAccount() {}
	
	public CurrentAccount(String name, double initialAccount) {
		this.name = name;
		this.initialAccount = initialAccount;
		this.acconuntNumber = ++accountCounter;
	}
	
	private String accountNumberFormat() {
		String initAccountNumber = "0000000000";
		String currentValue = String.valueOf(acconuntNumber);
		
		String subNumber = initAccountNumber.substring(0, initAccountNumber.length() - currentValue.length());
		return subNumber + currentValue;
	}
	
	public void setName (String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setInitialAccount(double initialAccount) {
		this.initialAccount = initialAccount;
	}
	
	public double getInitialAccount() {
		return initialAccount;
	}
	
	//ingresar dinero a la cuenta
	public void deposit (double money) {  
		this.initialAccount += money;
		
	}
	

	public boolean withdrawals (double money) {
			
		if(this.initialAccount >= money) { 
			this.initialAccount -= money; 
		
			return true;
		}else {
			
			return false;
		}
	}
	
	
	public String balanceInformation () {
		
		return "Saldo disponible: $" + this.getInitialAccount(); 
	}
	
	
	public boolean transfer(CurrentAccount anotherAccount, double money) {
		
		if(!this.equals(anotherAccount)) {
			
			boolean sucessExtract = this.withdrawals(money);
			
			if(sucessExtract) {
				anotherAccount.deposit(money);
				return true;
			}
		}
		
		return false;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + acconuntNumber;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CurrentAccount other = (CurrentAccount) obj;
		if (acconuntNumber != other.acconuntNumber)
			return false;
		return true;
	}
	
	

	public String getAcconuntNumber() {
		return this.accountNumberFormat();
	}

	@Override
	public String toString() {
		return "Cuenta bancaria N� : " + this.accountNumberFormat() + 
				"\nTitular: " + name + 
				"\nSaldo: " + initialAccount +
				"\n---------------------------------------------";
	}
	
	
	
}
