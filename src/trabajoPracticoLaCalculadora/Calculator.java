package trabajoPracticoLaCalculadora;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Calculator extends JFrame implements ActionListener {

	private JPanel panel = (JPanel) this.getContentPane();
	private JTextField viewfinder = new JTextField(); 
	
	private JButton btnC = new JButton("Borrar");
	private JButton btn7 = new JButton("7");
	private JButton btn8 = new JButton("8");
	private JButton btn9 = new JButton("9");
	private JButton btnX = new JButton("X");
	private JButton btn4 = new JButton("4");
	private JButton btn5 = new JButton("5");
	private JButton btn6 = new JButton("6");
	private JButton btnDec = new JButton("-");
	private JButton btn1 = new JButton("1");
	private JButton btn2 = new JButton("2");
	private JButton btn3 = new JButton("3");
	private JButton btnMore = new JButton("+");
	private JButton btnDiv = new JButton("/");
	private JButton btn0 = new JButton("0");
	private JButton btnPoint = new JButton(".");
	private JButton btnEqual = new JButton("=");
	
	public Calculator() {
		//Panel
		this.panel.setLayout(null);
		super.setSize(400,300);
		super.setVisible(true);
		super.setTitle("Calculadora");
		//Visor
		this.viewfinder.setBounds(10, 3, 355, 30);
		this.panel.add(viewfinder);
		//Botones
		this.btnC.setBounds(10, 38, 354, 40);
		this.panel.add(btnC);
		this.btnC.addActionListener(this);
		
		this.btn7.setBounds(10, 80, 83, 40);
		this.panel.add(btn7);
		this.btn7.addActionListener(this);
		this.btn8.setBounds(100, 80, 83, 40);
		this.panel.add(btn8);
		this.btn8.addActionListener(this);
		this.btn9.setBounds(190, 80, 83, 40);
		this.panel.add(btn9);
		this.btn9.addActionListener(this);
		this.btnX.setBounds(280, 80, 83, 40);
		this.panel.add(btnX);
		this.btnX.addActionListener(this);
		
		this.btn4.setBounds(10, 122, 83, 40);
		this.panel.add(btn4);
		this.btn4.addActionListener(this);
		this.btn5.setBounds(100, 122, 83, 40);
		this.panel.add(btn5);
		this.btn5.addActionListener(this);
		this.btn6.setBounds(190, 122, 83, 40);
		this.panel.add(btn6);
		this.btn6.addActionListener(this);
		this.btnDec.setBounds(280, 122, 83, 40);
		this.panel.add(btnDec);
		this.btnDec.addActionListener(this);
		
		this.btn1.setBounds(10, 164, 83, 40);
		this.panel.add(btn1);
		this.btn1.addActionListener(this);
		this.btn2.setBounds(100, 164, 83, 40);
		this.panel.add(btn2);
		this.btn2.addActionListener(this);
		this.btn3.setBounds(190, 164, 83, 40);
		this.panel.add(btn3);
		this.btn3.addActionListener(this);
		this.btnMore.setBounds(280, 164, 83, 40);
		this.panel.add(btnMore);
		this.btnMore.addActionListener(this);
		
		this.btnDiv.setBounds(10, 206, 83, 40);
		this.panel.add(btnDiv);
		this.btnDiv.addActionListener(this);
		this.btn0.setBounds(100, 206, 83, 40);
		this.panel.add(btn0);
		this.btn0.addActionListener(this);
		this.btnPoint.setBounds(190, 206, 83, 40);
		this.panel.add(btnPoint);
		this.btnPoint.addActionListener(this);
		this.btnEqual.setBounds(280, 206, 83, 40);
		this.panel.add(btnEqual);
		this.btnEqual.addActionListener(this);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == btn0) {
			
			if(viewfinder.getText() == "") {
				viewfinder.setText("0");	
			}else {
				viewfinder.setText(viewfinder.getText() + "0");
			}
			
		}
		
		if(e.getSource() == btn1) {
			
			if(viewfinder.getText() == "") {
				viewfinder.setText("1");	
			}else {
				viewfinder.setText(viewfinder.getText() + "1");
			}
			
		}
		
		if(e.getSource() == btn2) {
			
			if(viewfinder.getText() == "") {
				viewfinder.setText("2");	
			}else {
				viewfinder.setText(viewfinder.getText() + "2");
			}
			
		}
		
		if(e.getSource() == btn3) {
			
			if(viewfinder.getText() == "") {
				viewfinder.setText("3");	
			}else {
				viewfinder.setText(viewfinder.getText() + "3");
			}
			
		}
		
		if(e.getSource() == btn4) {
			
			if(viewfinder.getText() == "") {
				viewfinder.setText("4");	
			}else {
				viewfinder.setText(viewfinder.getText() + "4");
			}
			
		}
		
		if(e.getSource() == btn5) {
			
			if(viewfinder.getText() == "") {
				viewfinder.setText("5");	
			}else {
				viewfinder.setText(viewfinder.getText() + "5");
			}
			
		}
		
		if(e.getSource() == btn6) {
			
			if(viewfinder.getText() == "") {
				viewfinder.setText("6");	
			}else {
				viewfinder.setText(viewfinder.getText() + "6");
			}
			
		}
		
		if(e.getSource() == btn7) {
			
			if(viewfinder.getText() == "") {
				viewfinder.setText("7");	
			}else {
				viewfinder.setText(viewfinder.getText() + "7");
			}
			
		}
		
		if(e.getSource() == btn8) {
			
			if(viewfinder.getText() == "") {
				viewfinder.setText("8");	
			}else {
				viewfinder.setText(viewfinder.getText() + "8");
			}
			
		}
		
		if(e.getSource() == btn9) {
			
			if(viewfinder.getText() == "") {
				viewfinder.setText("9");	
			}else {
				viewfinder.setText(viewfinder.getText() + "9");
			}
			
		}
		
		if(e.getSource() == btnX) {
			
			if(viewfinder.getText() == "") {
				viewfinder.setText("*");	
			}else {
				viewfinder.setText(viewfinder.getText() + "*");
			}
			
		}
		
		if(e.getSource() == btnDec) {
			
			if(viewfinder.getText() == "") {
				viewfinder.setText("-");	
			}else {
				viewfinder.setText(viewfinder.getText() + "-");
			}
			
		}
		
		if(e.getSource() == btnMore) {
			
			if(viewfinder.getText() == "") {
				viewfinder.setText("+");	
			}else {
				viewfinder.setText(viewfinder.getText() + "+");
			}
			
		}
		
		if(e.getSource() == btnDiv) {
			
			if(viewfinder.getText() == "") {
				viewfinder.setText("/");	
			}else {
				viewfinder.setText(viewfinder.getText() + "/");
			}
			
		}
		
		if(e.getSource() == btnPoint) {
			
			if(viewfinder.getText() == "") {
				viewfinder.setText(".");	
			}else {
				viewfinder.setText(viewfinder.getText() + ".");
			}
			
		}
		
		if(e.getSource() == btnC) {
			viewfinder.setText("");
		}
		
		if(e.getSource() == btnEqual) {
			String num = viewfinder.getText();
			
			for(int i=0; i<num.length(); i++) {
				char character = num.charAt(i);
				
				if(character == '*') {
					String firstpart = num.substring(0,i);
					String secondpart = num.substring(i+1, num.length());
					
					double result = Double.parseDouble(firstpart) * Double.parseDouble(secondpart);
					viewfinder.setText(Double.toString(result));
				}
				
				if(character == '-') {
					String firstpart = num.substring(0,i);
					String secondpart = num.substring(i+1, num.length());
					
					double result = Double.parseDouble(firstpart) - Double.parseDouble(secondpart);
					viewfinder.setText(Double.toString(result));
				}
				
				if(character == '+') {
					String firstpart = num.substring(0,i);
					String secondpart = num.substring(i+1, num.length());
					
					double result = Double.parseDouble(firstpart) + Double.parseDouble(secondpart);
					viewfinder.setText(Double.toString(result));
				}
				
				if(character == '/') {
					String firstpart = num.substring(0,i);
					String secondpart = num.substring(i+1, num.length());
					
					double zero = Double.parseDouble(secondpart);
					
					if(zero == 0) {
						viewfinder.setText("ERROR EN LA DIVISI�N");
						
					}else {
					
						double result = Double.parseDouble(firstpart) / Double.parseDouble(secondpart);
						viewfinder.setText(Double.toString(result));
					}
					
				}
				
				
			}
		}
		
	}
	
	
	
}
