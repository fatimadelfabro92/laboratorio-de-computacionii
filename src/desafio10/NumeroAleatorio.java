package desafio10;

import java.util.Scanner;

public class NumeroAleatorio {
	public static void main (String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int nroAleatorio = (int) (Math.random()*100 +1);
		
		int intentos = 1; 
		int x;
		
		System.out.println("El sistema generar� de manera aleatoria un numero entre el 1 al 100, el juego consistira en adivinarlo en el menor numero de intentos posibles!"
				+ "\nBUENA SUERTE!");
		System.out.println("Ingrese un nro");
		x = sc.nextInt();
		
		
		do {
			
			if(nroAleatorio > x) {
				System.out.println("Ups! El nro ingresado es incorrecto."
				+ "\nPero no te rindas!" 
				+ "\n*Te damos una pista .. El nro 'X' es MAYOR al que ingresaste!");
				
			}else {
				System.out.println("Ups! El nro ingresado es incorrecto."
						+ "\nPero no te rindas!" 
						+ "\n*Te damos una pista .. El nro 'X' es MENOR al que ingresaste!");
				
			}
			
			if(x != nroAleatorio) {
				System.out.println("Intentalo de nuevo.. ingresa un nro ;)");
				x = sc.nextInt();
				intentos++;
			}
			
		}while(x != nroAleatorio);
		
		
		System.out.println("Correcto!. FELICITACIONES!!"
				+ "\nHas ganado con " + intentos + " intentos.");
	}

}
