package desafioIndividual6;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class MainFrame extends JFrame{
	
	private JPanel panel;

	public MainFrame () {
		this.setSize(500, 500);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		componentInitiator();
	}
	
	private void componentInitiator() {		
		createPanel();
		createBtn();
		createJTField();
	}

	private void createPanel() {
		this.panel = new JPanel();
		this.panel.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("Click!");
			}
		});
		this.getContentPane().add(panel);
	}
	
	private void createBtn() {
		JButton btn1 = new JButton();
		btn1.setBounds(200, 225, 100, 50);
		btn1.setText("Cerrar");
		btn1.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("Adios!!");
				System.exit(0);
				
				
			
			}
		});
	
		panel.add(btn1);
		
	}
	
	private void createJTField() {
		JTextField jtf = new JTextField(50);
		jtf.setBounds(150, 175, 200, 25);
		jtf.setVisible(true);
		jtf.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				//
				
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				System.out.println("El elemento ha ganado el foco.");
				
			}
		});
		panel.add(jtf);
	}
	
}
