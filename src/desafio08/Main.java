package desafio08;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {		
		
		double nro;
		
		Scanner sc = new Scanner(System.in);
		
		DecimalFormat df = new DecimalFormat("0.0#");

		System.out.println("Ingrese un nro para calcular la raiz cuadrada del mismo");
		
		nro = sc.nextDouble();
		
		System.out.println("La raiz cuadrada de su nro es: " + df.format(RaizScanner.calcularRaiz(nro)));
		
		
	}
}
